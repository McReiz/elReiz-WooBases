<!DOCTYPE html>
<html lang="es">
<head>
	<title>
	    
	<?php 
        if ( is_front_page()) :
            bloginfo( 'name' );
        else :
            wp_title();
        endif;
    ?>
	    
	</title>
	<!-- metatags -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php /*<!--<meta name="
    description" content="
    <?php bloginfo('description') ?>
    ">--> */?>

    <!-- link / scripts -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php echo themeb_dir('/favicon.ico') ?>">
    <?php wp_head(); ?>
</head>
<body <?php themeb_class_ct() ?>>
<div id="main-wrap">
    <header id="main-header">
        <div class="wrap">
            <div class="head-info" itemtype="http://schema.org/Person">
                <figure class="my-insign">
                    <img src="<?php echo themeb_dir('/Estilos/images/desing-in-hell-remaster.jpg');?>" alt="<?php bloginfo( 'name' ); ?>" itemprop="image">
                </figure>
                <div class="info">
                    <?php
                        if ( is_front_page() || is_home() ) :
                            ?><h1 class="webname" rel="home" itemprop="name"><a href="<?php bloginfo( 'url' ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1><?php
                        else :
                           ?><h2 class="webname" rel="home" itemprop="name"><a href="<?php bloginfo( 'url' ); ?> " itemprop="url"><?php bloginfo( 'name' ); ?></a></h2><?php
                        endif;
                        ?>
                    <span itemprop="jobTitle"><?php bloginfo('description'); ?></span>
                </div>
            </div><!-- end #head-info -->
            <nav class="head-menu">
               <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
            </nav>
        </div><!-- end #wrap -->
    </header>
    <?php //'child_of' => 6,'hide_empty' => 0
        
        if(is_home() || is_category() || is_single()):
            ?> <section class="categorys"><ul class="liss"><?php
            wp_list_categories(array(
                'title_li' => null,
                
            ));
        ?></ul>
            <ul class="sub-liss"></ul>
        </section><?php
        else:
            
        endif;
    ?>
    