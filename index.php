<?php
    get_header();
    ?>
        <main id="main-content">
            <?php
                get_template_part('content');
            ?>
        </main>
    <?php
    get_footer();
?>
