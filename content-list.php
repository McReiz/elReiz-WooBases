<article id="post-2" itemtype="http://schema.org/ImageObject">
    <div class="thumbnails">
    <?php
        if ( has_post_thumbnail() ) {
            ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php
                the_post_thumbnail('medium', array('itemprop' => 'contentUrl'));
            ?></a><?php
        }
        ?>
    </div>
        
    <div class="post-content">
        <header class="post-head">
        <div class="post-dates">
            <div class="items category">
                <div class="t-text fa fa-folder"></div>
                <div class="text"><?php the_category(' >> ','multiple'); ?> </div>
            </div>
            <div class="items blogger">
                <div class="t-text fa fa-user"></div>
                <div class="text" itemprop="author"> <?php the_author(); ?></div>
            </div>
            <div class="items fecha">
                <div class="t-text fa fa-calendar"></div>
                <time class="text" datetime="<?php the_time('j'); ?>-<?php the_time('F'); ?>-<?php the_time('Y')?>" itemprop="datePublished"><?php the_time('j'); ?> de <?php the_time(' F , Y'); ?></time>
            </div>
        </div>
        <h2 class="post-title" itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <div class="post" itemprop="description">
        <?php the_excerpt();?>
    </div>
    <footer class="post-footer">
        <div class="bottom-shared">
            <a href="http://facebook.com/sharer.php?u=<?php themeb_links() ?>" class="fb popup fa fa-facebook" target="_blank"></a>
            <a href="http://twitter.com/intent/tweet?text=<?php themeb_links('Twitter') ?>" class="tw popup fa fa-twitter" target="_blank"></a>
            <a href="https://plus.google.com/share?url=<?php themeb_links() ?>" class="gp popup fa fa-google-plus" target="_blank"></a>
        </div>
    </footer>
    </div>
</article>
