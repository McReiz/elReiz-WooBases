<article id="post">
    <div class="post-content">
        <div class="post">
            <?php
                if ( has_post_thumbnail() ) {
                    ?><div class="thumbnail-single"><?php
                        the_post_thumbnail();
                    ?></div><?php
                }
            ?>
        </div>
        <span class="content">
            <?php the_content();?>
        </span>
    </div>
</article>
