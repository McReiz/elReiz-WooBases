<?php
/*
    Template Name: Contacto
    Description: especial para contactos
*/

get_header();
?>
<main id="main-content">
    <section id="posts" <?php themeb_class_ct() ?>>
        <?php
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                if( is_single() || is_page() ):
                    get_template_part('/templates/page/contact');
                endif;
            endwhile;
        endif;
        ?>
    </section>
</main>
<?php
get_footer();