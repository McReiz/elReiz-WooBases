<section id="posts" <?php themeb_class_ct() ?> <?php theme_conditional(array('single' => 'itemtype="http://schema.org/CreativeWork"', 'list' => 'itemtype="http://schema.org/CreativeWork"'))?>>
    <!-- Header section -->
    <?php if( is_search() ) : ?>
        <header class="title-section">
            <?php get_search_form(); ?>
        </header>
    <?php endif; ?>
    <?php /*
        <?php elseif( is_archive() ) : ?>
        <header class="title-section">
            <?php the_archive_title(); ?>
        </header>
    <?php elseif( is_category() ): ?>
        <header class="title-section">
            <?php single_cat_title(); ?>
        </header>
        */ ?>
    <!-- fin header section -->
    <?php
        /* -- Tipos de contenidos -- */
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                if( is_front_page()):
                    get_template_part('content-home');
                elseif( is_single() ):
                    get_template_part('content-single');
                elseif( is_page() ):
                    get_template_part('content-page');
                /*elseif(is_category() || is_search() || is_archive()):
                    get_template_part('content-list');*/
                else:
                    get_template_part('content-list');
                endif;
            endwhile;
                the_posts_pagination( array(
                    'prev_text' => '<i class="fa fa-angle-double-left spaceRight"></i> Anterior',
                    'next_text' => 'Siguiente <i class="fa fa-angle-double-right spaceLeft"></i>',
                    'screen_reader_text' => ' '
                ) );
        else:
            echo wpautop( 'No hemos podido conseguir nada' );
        endif;
    ?>
</section>
